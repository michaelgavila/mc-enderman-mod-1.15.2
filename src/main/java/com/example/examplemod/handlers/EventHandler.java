package com.example.examplemod.handlers;

import com.example.examplemod.ExampleMod;
import com.example.examplemod.interfaces.IStoreStuff;
import com.example.examplemod.store.StoreStuffProvider;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.text.StringTextComponent;
import net.minecraftforge.event.entity.player.PlayerEvent;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.registries.ObjectHolder;

// TODO: This EventHandler is not being hit!
@Mod.EventBusSubscriber(modid = ExampleMod.MOD_ID, bus = Mod.EventBusSubscriber.Bus.MOD)
@ObjectHolder(ExampleMod.MOD_ID)
public class EventHandler
{
    @SubscribeEvent
    public static void onPlayerLogsIn(PlayerEvent.PlayerLoggedInEvent event)
    {
        PlayerEntity player = event.getPlayer();
        IStoreStuff storeStuff = (IStoreStuff) player.getCapability(StoreStuffProvider.STORE_STUFF_CAPABILITY, null);

        String message = String.format("Hello there, are you an enderman?", storeStuff.isEnder());
        player.sendMessage(new StringTextComponent(message));
    }

    @SubscribeEvent
    public static void onClick(PlayerInteractEvent.RightClickBlock event)
    {
        PlayerEntity player = event.getPlayer();
        IStoreStuff storeStuff = (IStoreStuff) player.getCapability(StoreStuffProvider.STORE_STUFF_CAPABILITY, null);
        String message = String.format("Hello there, are you an enderman?", storeStuff.isEnder());
        player.sendMessage(new StringTextComponent(message));
    }

}