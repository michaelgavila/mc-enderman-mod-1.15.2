package com.example.examplemod.handlers;

import com.example.examplemod.ExampleMod;
import com.example.examplemod.items.ItemMorpher;
import com.example.examplemod.items.TroySword;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemTier;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber.Bus;
import net.minecraftforge.registries.ObjectHolder;

@Mod.EventBusSubscriber(modid = ExampleMod.MOD_ID, bus = Bus.MOD)
@ObjectHolder(ExampleMod.MOD_ID)
public class ItemsHandler
{
    // Items are registered here, remember that item name MUST be exactly the same as in the registerItems method below.
    public static final TroySword troy_sword = null;
    public static final ItemMorpher morpher = null;
//    public static final Item troy_sword = null;

    @SubscribeEvent
    public static void registerItems(final RegistryEvent.Register<Item> event)
    {
        Item.Properties tsProps = getTroySwordProperties();
        Item.Properties morpherProps = getMorpherProperties();

        event.getRegistry().registerAll
        (
                // setRegistryName MÅÅ hete akkurat det same som Item field name!
            new TroySword(ItemTier.DIAMOND, 100, 10.0f, tsProps).setRegistryName("troy_sword"),
            new ItemMorpher(morpherProps).setRegistryName("morpher")
//            new Item(new Item.Properties().group(ItemGroup.COMBAT)).setRegistryName("troy_sword")
        );
    }

    private static Item.Properties getTroySwordProperties()
    {
        Item.Properties tsProps = new Item.Properties();
        tsProps.maxStackSize(1); // can't stack multiple damageable items
        tsProps.maxDamage(200);
        tsProps.defaultMaxDamage(190);
        tsProps.group(ItemGroup.COMBAT);

        return tsProps;
    }

    private static Item.Properties getMorpherProperties()
    {
        Item.Properties morpherProps = new Item.Properties();
        morpherProps.maxStackSize(1);
        morpherProps.setNoRepair();
        morpherProps.group(ItemGroup.COMBAT);

        return morpherProps;
    }
}
