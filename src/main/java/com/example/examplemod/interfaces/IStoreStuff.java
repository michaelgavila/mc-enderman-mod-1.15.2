package com.example.examplemod.interfaces;

public interface IStoreStuff {

    int tpcooldown();

    int meta();

    String blockName();

    boolean first();

    boolean isEnder();

    void setCooldown(int paramInt);

    void setFirst(boolean paramBoolean);

    void setBlockMeta(int paramInt);

    void setBlockName(String paramString);

    void setEnder(boolean paramBoolean);
}
