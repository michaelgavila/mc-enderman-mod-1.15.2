package com.example.examplemod.store;

import com.example.examplemod.interfaces.IStoreStuff;

public class StoreStuff implements IStoreStuff {

    private int tpcooldown = 0;

    private int meta = 0;

    private String block = "";

    private boolean first = false;

    private boolean ender;

    public int tpcooldown() {
        return this.tpcooldown;
    }

    public int meta() {
        return this.meta;
    }

    public boolean first() {
        return this.first;
    }

    public void setCooldown(int heartContainers) {
        this.tpcooldown = heartContainers;
    }

    public void setBlockMeta(int defaultHealth) {
        this.meta = defaultHealth;
    }

    public String blockName() {
        return this.block;
    }

    public boolean isEnder() {
        return this.ender;
    }

    public void setBlockName(String name) {
        this.block = name;
    }

    public void setFirst(boolean first) {
        this.first = first;
    }

    public void setEnder(boolean first) {
        this.ender = first;
    }
}
