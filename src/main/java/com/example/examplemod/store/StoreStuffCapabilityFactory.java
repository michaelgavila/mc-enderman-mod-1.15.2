package com.example.examplemod.store;

import com.example.examplemod.interfaces.IStoreStuff;
import java.util.concurrent.Callable;

public class StoreStuffCapabilityFactory implements Callable<IStoreStuff>
{
    @Override
    public IStoreStuff call() throws Exception
    {
        return new StoreStuff();
    }
}
