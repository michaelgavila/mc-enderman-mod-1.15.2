package com.example.examplemod.store;

import com.example.examplemod.interfaces.IStoreStuff;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.INBT;
import net.minecraft.util.Direction;
import net.minecraftforge.common.capabilities.Capability;

public class StoreStuffStorage implements Capability.IStorage<IStoreStuff>
{
    public INBT writeNBT(Capability<IStoreStuff> capability, IStoreStuff instance, Direction side)
    {
        CompoundNBT tag = new CompoundNBT();
        tag.putString("block", instance.blockName());
        tag.putInt("meta", instance.meta());
        tag.putInt("tpcooldown", instance.tpcooldown());
        tag.putBoolean("first", instance.first());
        tag.putBoolean("isender", instance.isEnder());
        return (INBT)tag;
    }

    public void readNBT(Capability<IStoreStuff> capability, IStoreStuff iStoreStuffInstance, Direction side, INBT nbt)
    {
        CompoundNBT tag = (CompoundNBT)nbt;
        iStoreStuffInstance.setCooldown(tag.getInt("tpcooldown"));
        iStoreStuffInstance.setBlockMeta(tag.getInt("meta"));
        iStoreStuffInstance.setBlockName(tag.getString("block"));
        iStoreStuffInstance.setFirst(tag.getBoolean("first"));
        iStoreStuffInstance.setEnder(tag.getBoolean("isEnder"));
    }
}
