package com.example.examplemod.store;

import com.example.examplemod.interfaces.IStoreStuff;
import net.minecraft.nbt.INBT;
import net.minecraft.util.Direction;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.CapabilityInject;
import net.minecraftforge.common.capabilities.ICapabilitySerializable;
import net.minecraftforge.common.util.LazyOptional;

import javax.annotation.Nonnull;

public class StoreStuffProvider implements ICapabilitySerializable<INBT>
{
    @CapabilityInject(IStoreStuff.class)
    public static final Capability<IStoreStuff> STORE_STUFF_CAPABILITY = null;

    private LazyOptional<IStoreStuff> instance = LazyOptional.of(STORE_STUFF_CAPABILITY::getDefaultInstance);

    @Override
    public <T> LazyOptional<T> getCapability(@Nonnull Capability<T> cap, Direction side)
    {
        return (cap == STORE_STUFF_CAPABILITY) ? this.instance.cast() : LazyOptional.empty();
    }

    public INBT serializeNBT()
    {
        return STORE_STUFF_CAPABILITY.getStorage().writeNBT(STORE_STUFF_CAPABILITY, this.instance.orElseThrow(() -> new IllegalArgumentException("LazyOptional must not be empty!")), null);
    }

    public void deserializeNBT(INBT nbt)
    {
        STORE_STUFF_CAPABILITY.getStorage().readNBT(STORE_STUFF_CAPABILITY, this.instance.orElseThrow(() -> new IllegalArgumentException("LazyOptional must not be empty!")), null, nbt);
    }
}
