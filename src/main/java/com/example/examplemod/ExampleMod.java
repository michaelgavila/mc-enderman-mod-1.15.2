package com.example.examplemod;

import com.example.examplemod.interfaces.IStoreStuff;
import com.example.examplemod.store.StoreStuffProvider;
import com.example.examplemod.store.StoreStuffStorage;
import com.example.examplemod.store.StoreStuff;
import net.minecraft.client.renderer.entity.EndermanRenderer;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.player.ClientPlayerEntity;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.monster.EndermanEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.BlockItemUseContext;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemUseContext;
import net.minecraft.pathfinding.PathType;
import net.minecraft.util.*;
import net.minecraft.util.math.*;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.client.event.RenderPlayerEvent;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.capabilities.CapabilityManager;
import net.minecraftforge.common.capabilities.ICapabilityProvider;
import net.minecraftforge.event.AttachCapabilitiesEvent;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.event.entity.EntityEvent;
import net.minecraftforge.event.entity.living.EnderTeleportEvent;
import net.minecraftforge.event.entity.living.LivingDamageEvent;
import net.minecraftforge.event.entity.living.LivingEvent;
import net.minecraftforge.event.entity.player.PlayerEvent;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import net.minecraftforge.event.world.BlockEvent;
import net.minecraftforge.eventbus.api.Event;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.InterModComms;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.event.lifecycle.InterModEnqueueEvent;
import net.minecraftforge.fml.event.lifecycle.InterModProcessEvent;
import net.minecraftforge.fml.event.server.FMLServerStartingEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import net.minecraftforge.registries.ForgeRegistries;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Random;
import java.util.stream.Collectors;

// The value here should match an entry in the META-INF/mods.toml file
@Mod("examplemod")
public class ExampleMod
{
    // Directly reference a log4j logger.
    private static final Logger LOGGER = LogManager.getLogger();

    public static final String MOD_ID = "examplemod";

    public ExampleMod() {
        // Register the setup method for modloading
        FMLJavaModLoadingContext.get().getModEventBus().addListener(this::setup);
        // Register the enqueueIMC method for modloading
        FMLJavaModLoadingContext.get().getModEventBus().addListener(this::enqueueIMC);
        // Register the processIMC method for modloading
        FMLJavaModLoadingContext.get().getModEventBus().addListener(this::processIMC);
        // Register the doClientStuff method for modloading
        FMLJavaModLoadingContext.get().getModEventBus().addListener(this::doClientStuff);

        // Register ourselves for server and other game events we are interested in
        MinecraftForge.EVENT_BUS.register(this);
    }

    @SubscribeEvent
    public  void onEntityCapabilityAttach(AttachCapabilitiesEvent<Entity> event) {
        if (event.getObject() instanceof PlayerEntity)
            event.addCapability(new ResourceLocation("examplemod", "tpcooldown"), (ICapabilityProvider) new StoreStuffProvider());
    }

    private void setup(final FMLCommonSetupEvent event)
    {
        // some preinit code
        LOGGER.info("HELLO FROM PREINIT");
        LOGGER.info("DIRT BLOCK >> {}", Blocks.DIRT.getRegistryName());

        CapabilityManager.INSTANCE.register(IStoreStuff.class, new StoreStuffStorage(), StoreStuff::new); // new StoreStuffCapabilityFactory()
    }

    private void doClientStuff(final FMLClientSetupEvent event) {
        // do something that can only be done on the client
        LOGGER.info("Got game settings {}", event.getMinecraftSupplier().get().gameSettings);
    }

    private void enqueueIMC(final InterModEnqueueEvent event)
    {
        // some example code to dispatch IMC to another mod
        InterModComms.sendTo("examplemod", "helloworld", () -> { LOGGER.info("Hello world from the MDK"); return "Hello world";});
    }

    private void processIMC(final InterModProcessEvent event)
    {
        // some example code to receive and process InterModComms from other mods
        LOGGER.info("Got IMC {}", event.getIMCStream().
                map(m->m.getMessageSupplier().get()).
                collect(Collectors.toList()));
    }

    // You can use SubscribeEvent and let the Event Bus discover methods to call
    @SubscribeEvent
    public void onServerStarting(FMLServerStartingEvent event) {
        // do something when the server starts
        LOGGER.info("HELLO from server starting");
    }

    // You can use EventBusSubscriber to automatically subscribe events on the contained class (this is subscribing to the MOD
    // Event bus for receiving Registry Events)
    @Mod.EventBusSubscriber(bus=Mod.EventBusSubscriber.Bus.MOD)
    public static class RegistryEvents {
        @SubscribeEvent
        public static void onBlocksRegistry(final RegistryEvent.Register<Block> blockRegistryEvent) {
            // register a new block here
            LOGGER.info("HELLO from Register Block");
        }
    }

    // Enderman code:
    public static boolean isEnder = true;
    public static int tpCooldown = 0;
    public static int tpCooldownTime = 5;
    public static float tpDistance = 180.0F;
    static Random rand = new Random();

    @SubscribeEvent
    public void onBlockPlaced(BlockEvent.EntityPlaceEvent event) { // TODO: ahdkjshakjdhsakjdhskjahdkjhsajdhsaj = onBlockPlaced?
        if (isEnder && event.getEntity() instanceof PlayerEntity) {
            PlayerEntity pe = (PlayerEntity)event.getEntity();
            Vec3d vec = new Vec3d((Vec3i)event.getBlockSnapshot().getPos());
            if (!event.getState().allowsMovement((IBlockReader)event.getWorld(), event.getBlockSnapshot().getPos(), PathType.AIR) && pe.getBoundingBox().intersects(vec.subtract(0.0D, 1.0D, 0.0D), vec.add(1.0D, 0.0D, 1.0D)))
                event.setCanceled(true);
        }
    }

    @SubscribeEvent
    public void onLivingHurt(LivingDamageEvent event) { // TODO: fungerer denne? (bruh)
        if (isEnder) {
            if (event.getSource().getImmediateSource() instanceof PlayerEntity) {
                // PlayerEntity pe = (PlayerEntity)event.getSource().getImmediateSource();
                event.setAmount(event.getAmount() + 6.0F);
            }
            if (event.getEntityLiving() instanceof PlayerEntity) {
                PlayerEntity pe = (PlayerEntity)event.getEntityLiving();
                DamageSource source = event.getSource();
                if (!pe.isInvulnerableTo(source) && (source == DamageSource.DROWN || source.isProjectile())) {
                    if (source != DamageSource.DROWN)
                        event.setCanceled(true);
                    double d0 = pe.getPosX() + (rand.nextDouble() - 0.5D) * 64.0D;
                    double d1 = pe.getPosY() + (rand.nextInt(64) - 32);
                    double d2 = pe.getPosZ() + (rand.nextDouble() - 0.5D) * 64.0D;
                    teleportTo(d0, d1, d2, pe, new EndermanEntity(EntityType.ENDERMAN, pe.getEntityWorld()));
                }
            }
        }
    }

    @SubscribeEvent
    public void onRenderPlayer(RenderPlayerEvent.Pre event) {
        if (isEnder) {
            try
            {
                EndermanEntity endermanEntity = new EndermanEntity(EntityType.ENDERMAN, event.getPlayer().getEntityWorld());
                endermanEntity.setRotationYawHead((event.getPlayer()).rotationYawHead);
                endermanEntity.setPositionAndRotation(event.getPlayer().getPosX(), event.getPlayer().getPosY(), event.getPlayer().getPosZ(), MathHelper.wrapDegrees((event.getPlayer()).rotationYaw), (event.getPlayer()).rotationPitch);
                endermanEntity.rotationYaw = MathHelper.wrapDegrees((event.getPlayer()).rotationYaw);
                endermanEntity.prevRenderYawOffset = (event.getPlayer()).prevRenderYawOffset;
                endermanEntity.renderYawOffset = (event.getPlayer()).renderYawOffset;
                endermanEntity.rotationPitch = (event.getPlayer()).rotationPitch;
                endermanEntity.prevRotationPitch = MathHelper.wrapDegrees((event.getPlayer()).prevRotationPitch);
                endermanEntity.prevRotationYaw = MathHelper.wrapDegrees((event.getPlayer()).prevRotationYaw);
                endermanEntity.prevRotationYawHead = (event.getPlayer()).prevRotationYawHead;
                if ((event.getPlayer()).swingingHand != null) {
                    endermanEntity.swingingHand = (event.getPlayer()).swingingHand;
                    endermanEntity.isSwingInProgress = true;
                    endermanEntity.swingProgress = (event.getPlayer()).swingProgress;
                    endermanEntity.prevSwingProgress = (event.getPlayer()).prevSwingProgress;
                }
                endermanEntity.hurtTime = (event.getPlayer()).hurtTime;
                IStoreStuff ss = event.getPlayer().getCapability(StoreStuffProvider.STORE_STUFF_CAPABILITY).orElse(null);
                if (!ss.blockName().isEmpty()) {
                    ForgeRegistries.BLOCKS.getValue(new ResourceLocation(ss.blockName()));
                    BlockState blocks = Block.getStateById(ss.meta());
                    endermanEntity.setHeldBlockState(blocks);
                }
                endermanEntity.deathTime = (event.getPlayer()).deathTime;
                endermanEntity.prevLimbSwingAmount = (event.getPlayer()).prevLimbSwingAmount;
                endermanEntity.limbSwingAmount = (event.getPlayer()).limbSwingAmount;
                endermanEntity.limbSwing = (event.getPlayer()).limbSwing;

                EndermanRenderer endermanRenderer = new EndermanRenderer(Minecraft.getInstance().getRenderManager());
                endermanRenderer.render(endermanEntity, MathHelper.wrapDegrees((event.getPlayer()).rotationYaw), event.getPartialRenderTick(), event.getMatrixStack(), event.getBuffers(), event.getLight());
//                Minecraft.getInstance().getRenderManager()
//                    .renderEntityStatic((Entity)endermanEntity, event.getPlayer().getPosX(), event.getPlayer().getPosY(), event.getPlayer().getPosZ(), MathHelper.wrapDegrees((event.getPlayer()).rotationYaw), event.getPartialRenderTick(), event.getMatrixStack(), event.getBuffers(), event.getLight());
                event.setCanceled(true);
            }
            catch (Exception ex)
            {
                System.out.println(ex.getMessage());
            }
        }
    }

    public static boolean empty(Vec3d pos, World world) {
        if (!world.getBlockState(new BlockPos(pos)).allowsMovement((IBlockReader)world, new BlockPos(pos), PathType.AIR))
            return false;
        if (!world.getBlockState(new BlockPos(pos.add(0.0D, 1.0D, 0.0D))).allowsMovement((IBlockReader)world, new BlockPos(pos.add(0.0D, 1.0D, 0.0D)), PathType.AIR))
            return false;
        if (!world.getBlockState(new BlockPos(pos.add(0.0D, 2.0D, 0.0D))).allowsMovement((IBlockReader)world, (new BlockPos(pos)).add(0, 2, 0), PathType.AIR))
            return false;
        return true;
    }

    @SubscribeEvent
    public void onRespawned(PlayerEvent.PlayerRespawnEvent event) {
        if (isEnder) {
            event.getPlayer().getAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(80.0D);
            event.getPlayer().setHealth(80.0F);
        }
    }

    // Teleports user a random distance:
    @SubscribeEvent
    public void onClickAir(PlayerInteractEvent.RightClickEmpty event) {
        if (isEnder) {
            ServerWorld sw = Minecraft.getInstance().getIntegratedServer().getWorld(event.getWorld().getDimension().getType());
            IStoreStuff ss = sw.getPlayerByUuid(event.getPlayer().getUniqueID()).getCapability(StoreStuffProvider.STORE_STUFF_CAPABILITY).orElse(null);
            ItemStack itemStack = event.getItemStack();
            PlayerEntity pe = event.getPlayer();
            Item playerMainHandHeldItem = pe.getHeldItemMainhand().getItem();
            Item playerOffHandHeldItem = pe.getHeldItemOffhand().getItem();
            String playerMainHandHeldItemName = playerMainHandHeldItem.toString(); // "morpher"

            if (playerMainHandHeldItem.toString() == "morpher" || playerOffHandHeldItem.toString() == "morpher")
                return;

            if (ss.tpcooldown() == 0 &&
                    itemStack.isEmpty() && !playerMainHandHeldItem.isFood() && !playerOffHandHeldItem.isFood()) {
                Vec3d pos = new Vec3d(pe.getPosX(), pe.getPosY() + pe.getEyeHeight(), pe.getPosZ());
                Vec3d prevpos = new Vec3d(pe.getPosX(), pe.getPosY() + pe.getEyeHeight(), pe.getPosZ());
                int a = 0;
                while (a < 12 && empty(pos, event.getWorld())) {
                    prevpos = pos.subtract(-Math.sin((pe.getRotationYawHead() / tpDistance) * Math.PI) * Math.cos((pe.rotationPitch / tpDistance) * Math.PI), -Math.sin((pe.rotationPitch / tpDistance) * Math.PI), Math.cos((pe.getRotationYawHead() / tpDistance) * Math.PI) * Math.cos((pe.rotationPitch / tpDistance) * Math.PI));
                    pos = pos.add(-Math.sin((pe.getRotationYawHead() / tpDistance) * Math.PI) * Math.cos((pe.rotationPitch / tpDistance) * Math.PI), -Math.sin((pe.rotationPitch / tpDistance) * Math.PI), Math.cos((pe.getRotationYawHead() / tpDistance) * Math.PI) * Math.cos((pe.rotationPitch / tpDistance) * Math.PI));
                    a++;
                }
                Vec3d posb = prevpos;
                while (posb.getY() > 0.0D && !pe.world.getBlockState(new BlockPos(posb)).getMaterial().blocksMovement())
                    posb = posb.subtract(0.0D, 1.0D, 0.0D);
                posb = posb.add(0.0D, 1.0D, 0.0D);
                pe.velocityChanged = true;
                pe.noClip = true;
                PlayerEntity spe = sw.getWorld().getPlayerByUuid(event.getPlayer().getUniqueID());
                BlockPos bp = new BlockPos(posb.getX(), posb.getY(), posb.getZ());
                spe.setPositionAndRotation(bp.getX(), bp.getY(), bp.getZ(), (pe.getPitchYaw()).y, (pe.getPitchYaw()).x);
                pe.setPositionAndRotation(bp.getX(), bp.getY(), bp.getZ(), (pe.getPitchYaw()).y, (pe.getPitchYaw()).x);
                tpCooldown += tpCooldownTime;
                ss.setCooldown(tpCooldown);
                pe.noClip = false;
            }
        }
    }

    // Shift + høyre-klikk for å løfte opp en blokk, shift + høyre-klikk for å plassere den igjen:
    @SubscribeEvent
    public void onClick(PlayerInteractEvent.RightClickBlock event) {
        if (isEnder) {
            ServerWorld sw = Minecraft.getInstance().getIntegratedServer().getWorld(event.getWorld().getDimension().getType());
            BlockState bs = event.getWorld().getBlockState(event.getPos());
            if (event.getHand() == event.getPlayer().getActiveHand())
                if (!event.getPlayer().isShiftKeyDown()) {
                    if (!bs.onBlockActivated(event.getWorld(), event.getPlayer(), event.getHand(), new BlockRayTraceResult(Vec3d.ZERO, event.getFace(), event.getPos(), false)).isSuccess()) { // TODO: func_215687_a = onBlockActivated? Måtte legge til .isSuccess() til slutt...
                        if (event.getItemStack().isEmpty()) {
                            IStoreStuff ss = event.getPlayer().getCapability(StoreStuffProvider.STORE_STUFF_CAPABILITY).orElse(null);
                            if (!event.getPlayer().isShiftKeyDown() &&
                                    ss.tpcooldown() == 0) {
                                PlayerEntity pe = event.getPlayer();
                                Vec3d pos = new Vec3d(pe.getPosX(), pe.getPosY() + pe.getEyeHeight(), pe.getPosZ());
                                Vec3d prevpos = new Vec3d(pe.getPosX(), pe.getPosY() + pe.getEyeHeight(), pe.getPosZ());
                                int a = 0;
                                while (a < 12 && empty(pos, event.getWorld())) {
                                    prevpos = pos.subtract(-Math.sin((pe.getRotationYawHead() / 180.0F) * Math.PI) * Math.cos((pe.rotationPitch / 180.0F) * Math.PI), -Math.sin((pe.rotationPitch / 180.0F) * Math.PI), Math.cos((pe.getRotationYawHead() / 180.0F) * Math.PI) * Math.cos((pe.rotationPitch / 180.0F) * Math.PI));
                                    pos = pos.add(-Math.sin((pe.getRotationYawHead() / 180.0F) * Math.PI) * Math.cos((pe.rotationPitch / 180.0F) * Math.PI), -Math.sin((pe.rotationPitch / 180.0F) * Math.PI), Math.cos((pe.getRotationYawHead() / 180.0F) * Math.PI) * Math.cos((pe.rotationPitch / 180.0F) * Math.PI));
                                    a++;
                                }
                                Vec3d posb = prevpos;
                                while (posb.getY() > 0.0D && !pe.world.getBlockState(new BlockPos(posb)).getMaterial().blocksMovement())
                                    posb = posb.subtract(0.0D, 1.0D, 0.0D);
                                posb = posb.add(0.0D, 1.0D, 0.0D);
                                pe.velocityChanged = true;
                                pe.noClip = true;
                                PlayerEntity spe = sw.getWorld().getPlayerByUuid(event.getPlayer().getUniqueID());
                                BlockPos bp = new BlockPos(posb.getX(), posb.getY(), posb.getZ());
                                spe.setPositionAndRotation(bp.getX(), bp.getY(), bp.getZ(), (pe.getPitchYaw()).y, (pe.getPitchYaw()).x);
                                pe.setPositionAndRotation(bp.getX(), bp.getY(), bp.getZ(), (pe.getPitchYaw()).y, (pe.getPitchYaw()).x);
                                tpCooldown = 30;
                                ss.setCooldown(tpCooldown);
                                pe.noClip = false;
                            }
                        }
                    } else {
                        event.setCanceled(true);
                    }
                } else if (event.getItemStack().isEmpty()) {
                    PlayerEntity pe = event.getPlayer();
                    ClientPlayerEntity clientPlayerEntity = (Minecraft.getInstance()).player;
                    event.setCanceled(true);
                    IStoreStuff ss = (StoreStuff) event.getPlayer().getCapability(StoreStuffProvider.STORE_STUFF_CAPABILITY, null).orElse(null);
                    if (!ss.blockName().isEmpty()) {
                        ForgeRegistries.BLOCKS.getValue(new ResourceLocation(ss.blockName()));
                        BlockState blocks = Block.getStateById(ss.meta());
                        BlockPos.Mutable blockpos$mutableblockpos = new BlockPos.Mutable(event.getPos());
                        ItemUseContext ius = new ItemUseContext(pe, event.getHand(), new BlockRayTraceResult(Vec3d.ZERO, event.getFace(), (BlockPos)blockpos$mutableblockpos, false));
                        if (event.getWorld().getBlockState((BlockPos)blockpos$mutableblockpos).isReplaceable(new BlockItemUseContext(ius))) {
                            Vec3d vec = new Vec3d((Vec3i)blockpos$mutableblockpos);
                            if (blocks.allowsMovement((IBlockReader)event.getWorld(), (BlockPos)blockpos$mutableblockpos, PathType.AIR) || !pe.getBoundingBox().intersects(vec.subtract(0.0D, 1.0D, 0.0D), vec.add(1.0D, 0.0D, 1.0D))) {
                                event.getWorld().setBlockState((BlockPos)blockpos$mutableblockpos, blocks);
                                ss.setBlockName("");
                                ss.setBlockMeta(0);
                                if (event.getHand() != null) {
                                    if ((clientPlayerEntity.getEntityWorld()).isRemote) {
                                        clientPlayerEntity.setActiveHand(event.getHand());
                                        clientPlayerEntity.swingArm(event.getHand());
                                    }
                                } else {
                                    clientPlayerEntity.setActiveHand(Hand.MAIN_HAND);
                                    clientPlayerEntity.swingArm(Hand.MAIN_HAND);
                                }
                            }
                        } else {
                            blockpos$mutableblockpos.move(event.getFace());
                            ius = new ItemUseContext(pe, event.getHand(), new BlockRayTraceResult(Vec3d.ZERO, event.getFace(), (BlockPos)blockpos$mutableblockpos, false));
                            if (event.getWorld().getBlockState((BlockPos)blockpos$mutableblockpos).isReplaceable(new BlockItemUseContext(ius))) {
                                Vec3d vec = new Vec3d((Vec3i)blockpos$mutableblockpos);
                                if (blocks.allowsMovement((IBlockReader)event.getWorld(), (BlockPos)blockpos$mutableblockpos, PathType.AIR) || !pe.getBoundingBox().intersects(vec.subtract(0.0D, 1.0D, 0.0D), vec.add(1.0D, 0.0D, 1.0D))) {
                                    event.getWorld().setBlockState((BlockPos)blockpos$mutableblockpos, blocks);
                                    ss.setBlockName("");
                                    ss.setBlockMeta(0);
                                    if (event.getHand() != null) {
                                        if ((clientPlayerEntity.getEntityWorld()).isRemote) {
                                            clientPlayerEntity.setActiveHand(event.getHand());
                                            clientPlayerEntity.swingArm(event.getHand());
                                        }
                                    } else {
                                        clientPlayerEntity.setActiveHand(Hand.MAIN_HAND);
                                        clientPlayerEntity.swingArm(Hand.MAIN_HAND);
                                    }
                                }
                            }
                        }
                    } else {
                        BlockState state = event.getWorld().getBlockState(event.getPos());
                        if (!state.hasTileEntity() && state.getBlockHardness((IBlockReader)event.getWorld(), event.getPos()) != -1.0F) {
                            event.getWorld().setBlockState(event.getPos(), Blocks.AIR.getDefaultState());
                            ss.setBlockName(state.getBlock().getRegistryName().toString());
                            state.getBlock();
                            ss.setBlockMeta(Block.getStateId(state));
                            if (event.getHand() != null) {
                                if ((clientPlayerEntity.getEntityWorld()).isRemote) {
                                    clientPlayerEntity.setActiveHand(event.getHand());
                                    clientPlayerEntity.swingArm(event.getHand());
                                }
                            } else {
                                clientPlayerEntity.setActiveHand(Hand.MAIN_HAND);
                                clientPlayerEntity.swingArm(Hand.MAIN_HAND);
                            }
                        }
                    }
                }
        }
    }

    @SubscribeEvent
    public void onLivingUpdate(LivingEvent.LivingUpdateEvent event) {
        if (event.getEntityLiving() instanceof PlayerEntity) {
            PlayerEntity pe = (PlayerEntity)event.getEntityLiving();
            IStoreStuff ss = (IStoreStuff)pe.getCapability(StoreStuffProvider.STORE_STUFF_CAPABILITY).orElse(null);
            isEnder = ss.isEnder();
            pe.recalculateSize();
            if (!ss.first() && ss.isEnder()) {
                pe.getAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(80.0D);
                pe.setHealth(80.0F);
                ss.setFirst(true);
            }
            if (!(pe.getEntityWorld()).isRemote) {
                if (isEnder)
                    pe.getAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(80.0D);
                if (ss.tpcooldown() > 0)
                    ss.setCooldown(ss.tpcooldown() - 1);
            }
            if (isEnder) {
                if (pe.getBoundingBox().getYSize() < 2.9D && !pe.isSwimming() && !pe.isElytraFlying() && !pe.isSpinAttacking())
                    pe.setBoundingBox(pe.getBoundingBox().expand(0.0D, 2.9D - pe.getBoundingBox().getYSize(), 0.0D));
                if (pe.isInWaterRainOrBubbleColumn() &&
                        !pe.isInvulnerable())
                    if (pe.attackEntityFrom(DamageSource.DROWN, 1.0F)) {
                        double d0 = pe.getPosX() + (rand.nextDouble() - 0.5D) * 64.0D;
                        double d1 = pe.getPosY() + (rand.nextInt(64) - 32);
                        double d2 = pe.getPosZ() + (rand.nextDouble() - 0.5D) * 64.0D;
                        teleportTo(d0, d1, d2, pe, new EndermanEntity(EntityType.ENDERMAN, pe.getEntityWorld()));
                    }
            }
        }
    }

    private static boolean teleportTo(double x, double y, double z, PlayerEntity pe, EndermanEntity ee) {
        BlockPos.Mutable blockpos$mutableblockpos = new BlockPos.Mutable(x, y, z);
        while (blockpos$mutableblockpos.getY() > 0 && !pe.world.getBlockState((BlockPos)blockpos$mutableblockpos).getMaterial().blocksMovement())
            blockpos$mutableblockpos.move(Direction.DOWN);
        if (!pe.world.getBlockState((BlockPos)blockpos$mutableblockpos).getMaterial().blocksMovement()) {
            while (blockpos$mutableblockpos.getY() < 256 && !pe.world.getBlockState((BlockPos)blockpos$mutableblockpos).getMaterial().blocksMovement())
                blockpos$mutableblockpos.move(Direction.UP);
            if (!pe.world.getBlockState((BlockPos)blockpos$mutableblockpos).getMaterial().blocksMovement())
                return false;
            EnderTeleportEvent enderTeleportEvent = new EnderTeleportEvent((LivingEntity)ee, x, y, z, 0.0F);
            if (MinecraftForge.EVENT_BUS.post((Event)enderTeleportEvent))
                return false;
            boolean bool = pe.attemptTeleport(enderTeleportEvent.getTargetX(), enderTeleportEvent.getTargetY(), enderTeleportEvent.getTargetZ(), true);
            if (bool) {
                pe.world.playSound((PlayerEntity)null, pe.prevPosX, pe.prevPosY, pe.prevPosZ, SoundEvents.ENTITY_ENDERMAN_TELEPORT, pe.getSoundCategory(), 1.0F, 1.0F);
                pe.playSound(SoundEvents.ENTITY_ENDERMAN_TELEPORT, 1.0F, 1.0F);
            }
            return bool;
        }
        EnderTeleportEvent event = new EnderTeleportEvent((LivingEntity)ee, x, y, z, 0.0F);
        if (MinecraftForge.EVENT_BUS.post((Event)event))
            return false;
        boolean flag = pe.attemptTeleport(event.getTargetX(), event.getTargetY(), event.getTargetZ(), true);
        if (flag) {
            pe.world.playSound((PlayerEntity)null, pe.prevPosX, pe.prevPosY, pe.prevPosZ, SoundEvents.ENTITY_ENDERMAN_TELEPORT, pe.getSoundCategory(), 1.0F, 1.0F);
            pe.playSound(SoundEvents.ENTITY_ENDERMAN_TELEPORT, 1.0F, 1.0F);
        }
        return flag;
    }

    @SubscribeEvent
    public void onEntityConstructing(EntityEvent.EyeHeight event) {
        if (event.getEntity() instanceof PlayerEntity)
            if (isEnder) {
                if (event.getOldHeight() == 0.4F) {
                    event.setNewHeight(0.4F);
                } else if (event.getOldHeight() == 1.27F) {
                    event.setNewHeight(2.0F);
                } else if (event.getOldHeight() == 1.62F) {
                    event.setNewHeight(2.45F);
                }
            } else if (event.getOldHeight() == 0.4F) {
                event.setNewHeight(0.4F);
            } else if (event.getOldHeight() == 2.9F) {
                event.setNewHeight(1.27F);
            } else if (event.getOldHeight() == 2.45F) {
                event.setNewHeight(1.62F);
            }
    }

}
