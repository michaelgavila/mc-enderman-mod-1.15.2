package com.example.examplemod.items;

import com.google.common.collect.Multimap;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.material.Material;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.attributes.AttributeModifier;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.*;
import net.minecraft.tags.BlockTags;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class TroySword extends SwordItem
{
    private final float attackDamage;
    private final float attackSpeed;

    public TroySword(IItemTier itemTier, int attDmg, float attSpd, Properties properties)
    {
        super(itemTier, attDmg, attSpd, properties);
        this.attackSpeed = attSpd;
        this.attackDamage = (float)attDmg + itemTier.getAttackDamage();
    }

    public float getAttackDamage()
    {
        return this.attackDamage;
    }

    public boolean canPlayerBreakBlockWhileHolding(BlockState blockState, World world, BlockPos blockPos, PlayerEntity playerEntity)
    {
        return !playerEntity.isCreative();
    }

    public float getDestroySpeed(ItemStack itemStack, BlockState blockState)
    {
        Block block = blockState.getBlock();
        if (block == Blocks.COBWEB)
        {
            return 15.0F;
        }
        else
        {
            Material material = blockState.getMaterial();
            return material != Material.PLANTS &&
                   material != Material.TALL_PLANTS &&
                   material != Material.CORAL &&
                   !blockState.isIn(BlockTags.LEAVES) &&
                   material != Material.GOURD ? 1.0F : 1.5F;
        }
    }

    public boolean hitEntity(ItemStack itemStack, LivingEntity entity1, LivingEntity entity2)
    {
        itemStack.damageItem(1, entity2, (p_220045_0_) -> {
            p_220045_0_.sendBreakAnimation(EquipmentSlotType.MAINHAND);
        });
        return true;
    }

    public boolean onBlockDestroyed(ItemStack itemStack, World world, BlockState blockState, BlockPos blockPos, LivingEntity livingEntity)
    {
        if (blockState.getBlockHardness(world, blockPos) != 0.0F) {
            itemStack.damageItem(2, livingEntity, (p_220044_0_) -> {
                p_220044_0_.sendBreakAnimation(EquipmentSlotType.MAINHAND);
            });
        }
        return true;
    }

    public boolean canHarvestBlock(BlockState blockState)
    {
        return blockState.getBlock() == Blocks.COBWEB;
    }

    public Multimap<String, AttributeModifier> getAttributeModifiers(EquipmentSlotType equipmentSlotType)
    {
        Multimap<String, AttributeModifier> attributeModifiers = super.getAttributeModifiers(equipmentSlotType);

        if (equipmentSlotType == EquipmentSlotType.MAINHAND) {
            attributeModifiers.put(SharedMonsterAttributes.ATTACK_DAMAGE.getName(), new AttributeModifier(ATTACK_DAMAGE_MODIFIER, "Weapon modifier", (double)this.attackDamage, AttributeModifier.Operation.ADDITION));
            attributeModifiers.put(SharedMonsterAttributes.ATTACK_SPEED.getName(), new AttributeModifier(ATTACK_SPEED_MODIFIER, "Weapon modifier", (double)this.attackSpeed, AttributeModifier.Operation.ADDITION));
        }

        return attributeModifiers;
    }
}
