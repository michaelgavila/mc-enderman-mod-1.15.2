package com.example.examplemod.items;

import java.util.ArrayList;
import java.util.Collection;

import com.example.examplemod.interfaces.IStoreStuff;
import com.example.examplemod.store.StoreStuffProvider;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ActionResult;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Hand;
import net.minecraft.world.World;

public class ItemMorpher extends Item
{
    public ItemMorpher(Item.Properties properties)
    {
        super(properties);
    }

    public ActionResult<ItemStack> onItemRightClick(World worldIn, PlayerEntity playerEntity, Hand hand)
    {
        ItemStack itemStack = playerEntity.getHeldItem(hand); // you can reduce the itemstack with 1 by calling itemStack.shrink(1);
        IStoreStuff ss = (IStoreStuff) playerEntity.getCapability(StoreStuffProvider.STORE_STUFF_CAPABILITY).orElse(null);
        if (ss != null) {
            ss.setEnder(!ss.isEnder());

            if (ss.isEnder())
            {
                playerEntity.getAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(80.0D);
                playerEntity.setHealth(playerEntity.getHealth() * 4.0F); // enderman's health is 4 times larger than the player's health
            }
            else
            {
                playerEntity.getAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(20.0D);
                playerEntity.setHealth(playerEntity.getHealth() / 4.0F); // player health is 4 times smaller than enderman's health
            }
            return new ActionResult(ActionResultType.SUCCESS, itemStack);
        }
        return new ActionResult(ActionResultType.FAIL, itemStack);
    }

    public Collection<ItemGroup> getCreativeTabs()
    {
        Collection<ItemGroup> oi = new ArrayList<>();
        oi.add(ItemGroup.TRANSPORTATION); // field_78027_g
        oi.add(ItemGroup.SEARCH); // field_78026_f
        return oi;
    }
}
