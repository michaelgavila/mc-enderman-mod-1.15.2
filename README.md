troy-sverd

Steg 1 - Brukte en decompiler til å få .class filene til Enderman mod fra 2012.
Steg 2 - Brukte Minecraft Deobfuscator 3.0 til å søke opp funksjonsnavn som var obfuscated og manuelt bytte de i til ordentlige Minecraft funksjonsnavn.
Steg 3 - Forbedret de funksjonene som ikke fungerte i nyeste versjon av Minecraft 1.15.2.
Steg 4 - Profit!